from django.conf.urls import patterns, include, url

from board import views

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<board_id>\d+)/$', views.board, name='board'),
    url(r'^move/(?P<board_id>\d+)/$', views.make_move, name='move'),
)
