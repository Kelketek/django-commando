from django.db import models
import datetime
from django.utils import timezone

def get_name_of_piece_type(piece_type):
    if piece_type == 1:
        return 'light infantry'
    if piece_type == 2:
        return 'heavy infantry'
    if piece_type == 3:
        return 'cavalry'
    raise Error('invalid piece type')

class Board(models.Model):
    #width = models.IntegerField()
    #height = models.IntegerField()
    player_to_move = models.IntegerField(default=1)
    
    def has_pieces_at(self, x, y):
        return len(self.get_pieces_at(x, y)) > 0
    def get_pieces_at(self, x, y):
        return Piece.objects.filter(board=self.pk, x=x, y=y)
    def get_pieces(self):
        return Piece.objects.filter(board=self.pk)
        
    def get_width(self):
        return 8
    def get_height(self):
        return 8
    
    def try_move(self, from_x, from_y, to_x, to_y):
        pieces = self.get_pieces_at(from_x, from_y)
        if len(pieces) == 0:
            return (False, 'No piece selected.')
        return pieces[0].try_move(to_x, to_y)

class Color:
    ORANGE = 1
    BLUE = 2
    
class PieceType:
    LIGHT_INFANTRY = 1
    HEAVY_INFANTRY = 2
    CAVALRY = 3
    
class Piece(models.Model):
    board = models.ForeignKey(Board)
    piece_type = models.IntegerField()
    x = models.IntegerField()
    y = models.IntegerField()
    owner = models.IntegerField() # 1 or 2
    
    def __unicode__(self):
        return get_name_of_piece_type(self.piece_type)
        
    def get_type_name(self):
        return get_name_of_piece_type(self.piece_type)
        
    def count_adjacent_enemies(self):
        raise NotImplementedError()
        
    def try_move(self, dest_x, dest_y):
        if self.board.player_to_move != self.owner:
            return (False, "It's not that player's turn to move.")
        if int(dest_x) == self.x and int(dest_y) == self.y:
            return (False, "Can't move in place.")
        if self.board.has_pieces_at(dest_x, dest_y):
            return (False, "Can't move onto another piece.")
        self.x = dest_x
        self.y = dest_y
        self.save()
        if self.board.player_to_move == Color.ORANGE:
            self.board.player_to_move = Color.BLUE
        else:
            self.board.player_to_move = Color.ORANGE
        self.board.save()
        return (True, None)
        