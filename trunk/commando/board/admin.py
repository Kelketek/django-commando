from django.contrib import admin
from board.models import Board, Piece

class PieceInline(admin.TabularInline):
    model = Piece
    extra = 3

class BoardAdmin(admin.ModelAdmin):
    inlines = [PieceInline]

admin.site.register(Board, BoardAdmin)
