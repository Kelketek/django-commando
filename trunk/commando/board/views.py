from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.contrib import messages

from board.models import Board, Piece, PieceType, Color

class IndexView(generic.ListView):
    template_name = 'board/index.html'
    context_object_name = 'board_list'

    def get_queryset(self):
        return Board.objects.all()
        
def make_move(request, board_id):
    if request.method == 'POST':
        board = get_object_or_404(Board, pk=board_id)
        from_x = request.POST['from_x']
        from_y = request.POST['from_y']
        to_x = request.POST['to_x']
        to_y = request.POST['to_y']
        
        success, fail_reason = board.try_move(from_x, from_y, to_x, to_y)
        if not success:
            messages.error(request, fail_reason)
        """#piece = board.get_pieces_at(from_x, from_y)[0]
        if piece:
            #piece.x = to_x
            #piece.y = to_y
            piece.try_move(to_x, to_y)
            piece.save()
        else:
            message = 'Invalid piece selected'"""
        #return HttpResponseRedirect(reverse('board:board', kwargs={ 'board_id': board_id, 'message':fail_reason }))
        return redirect('board:board', board_id)
        
def board(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    
    rows = []
    for y in range(0, board.get_height()):
        rows.append([])
        for x in range(0, board.get_width()):
            pieces = board.get_pieces_at(x, y)
            if len(pieces) == 0:
                rows[y].append(None)
            else:
                piece = pieces[0]
                if piece.owner == 1:
                    color = 'orange'
                elif piece.owner == 2:
                    color = 'blue'
                else:
                    raise Error('Invalid piece owner')
                    
                if board.player_to_move == Color.ORANGE:
                    to_move = 'Orange to move.'
                elif board.player_to_move == Color.BLUE:
                    to_move = 'Blue to move.'
                    
                if piece.piece_type == PieceType.LIGHT_INFANTRY:
                    rows[y].append('board/images/' + color + '-light.png')
                elif piece.piece_type == PieceType.HEAVY_INFANTRY:
                    rows[y].append('board/images/' + color + '-heavy.png')
                elif piece.piece_type == PieceType.CAVALRY:
                    rows[y].append('board/images/' + color + '-cavalry.png')
                else:
                    raise Error('Invalid piece type')
                    
    return render(request, 'board/board.html', { 'rows':rows, 'to_move':to_move, 'board':board })
    